// Copyright 2022 Roma Hicks

// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:

// 1. Redistributions of source code must retain the above copyright notice, this
// list of conditions and the following disclaimer.

// 2. Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.

// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

package main

import (
	"net/http"
	"reflect"
	"time"

	geopackage "gitlab.com/feather-wfst/geopackagego"
	"gitlab.com/feather-wfst/wfst-server/xmllibrary"
)

// FeatherServer implements the FeatherWFST/server interface and is the top
// most structure for the FeatherWFST application.
type FeatherServer struct {
	httpServer          *http.Server
	database            *geopackage.GeoPackage
	databasePath        string
	startTime           time.Time
	ServerTitle         string
	ServerAbstract      string
	ServiceType         string
	ServiceTypeVersion  string
	UpdateSequence      string
	GeometryOperands    []string
	SpatialOperators    []string
	ComparisonOperators []string
	IDOperands          []string
	Operations          xmllibrary.OperationsMetadata
}

// GetCapabilities creates a XML object that can be returned to the
// client describing the Web Feature Server's capabilities.
func (fS *FeatherServer) GetCapabilities() string {
	template, err := xmllibrary.BuildWFSCapabilitiesString(fS)
	if err != nil {
		panic(err)
	}
	//xmllibrary.BuildFeatureTypeList(template, fS.database.Content)
	return template
}

// ListContent returns a GeoPackageGo structure listing the currently
// available layers on the server.
func (fS *FeatherServer) ListContent() []*geopackage.Content {
	return fS.database.Content
}

// Uptime returns the time elasped since the server started.
func (fS *FeatherServer) Uptime() time.Duration {
	return time.Since(fS.startTime)
}

// GeoPackagePath returns an absolute path to the GeoPackage in use by
// the server.
func (fS *FeatherServer) GeoPackagePath() string {
	return fS.databasePath
}

// ListenAddress lists the address the server (the REST server) is listening
// for incoming requests upon.
func (fS *FeatherServer) ListenAddress() string {
	return fS.httpServer.Addr
}

// CalcLayerSize returns the number of features per layer and their approximate
// memory consumption.
func (fS *FeatherServer) CalcLayerSize(layer *geopackage.Content) uint {
	var sizeByte uintptr
	sizeByte += reflect.TypeOf(*layer).Size()
	for _, c := range layer.Data.Columns {
		sizeByte += reflect.TypeOf(c).Size()
	}
	for _, r := range layer.Data.Rows {
		sizeByte += reflect.TypeOf(*r).Size()
		for _, d := range r.Data {
			sizeByte += reflect.TypeOf(d).Size()
		}
		for _, f := range r.Flags {
			sizeByte += reflect.TypeOf(f).Size()
		}
	}
	return uint(sizeByte)
}

// xmllibrary WFSServerInterface functinons.
// Send the required information to the XML library to create a WFS_Capabilities document.

// GetServerTitle sends the public name of the WFS server, this case FeatherWFST.
func (fS *FeatherServer) GetServerTitle() string {
	return fS.ServerTitle
}

func (fS *FeatherServer) GetServerAbstract() string {
	return fS.ServerAbstract
}

func (fS *FeatherServer) GetServiceType() string {
	return fS.ServiceType
}

func (fS *FeatherServer) GetServiceTypeVersion() string {
	return fS.ServiceTypeVersion
}

func (fS *FeatherServer) GetGeometryOperands() []string {
	return fS.GeometryOperands
}

func (fS *FeatherServer) GetComparisonOperators() []string {
	return fS.ComparisonOperators
}

func (fS *FeatherServer) GetSpatialOperators() []string {
	return fS.SpatialOperators
}

func (fS *FeatherServer) GetIDOperands() []string {
	return fS.IDOperands
}

func (fS *FeatherServer) GetOperationsMetadata() xmllibrary.OperationsMetadata {
	return fS.Operations
}
