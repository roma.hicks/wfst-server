// WFST-Server Test Suite by Roma Hicks is marked with CC0 1.0 Universal.
// To view a copy of this license, visit http://creativecommons.org/publicdomain/zero/1.0
package main

import (
	"bytes"
	"net/http"
	"testing"

	xsdvalidate "github.com/terminalstatic/go-xsd-validate"
)

func TestGetCapabilitiesXML(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	xsdvalidate.Init()
	defer xsdvalidate.Cleanup()

	handler, err := xsdvalidate.NewXsdHandlerUrl("http://schemas.opengis.net/wfs/1.1.0/wfs.xsd",
		xsdvalidate.ParsErrDefault)
	defer handler.Free()
	if err != nil {
		t.Fatal(err)
	}

	t.Run("Validate recieved XML against OpenGIS WFS XSD.", func(t *testing.T) {
		c := &http.Client{}
		res, err := c.Get("http://127.0.0.1:6060/wfs?service=WFS&request=GetCapabilities")
		if err != nil {
			t.Fatal(err)
		}

		buf := bytes.NewBuffer(make([]byte, 0))
		_, err = buf.ReadFrom(res.Body)
		if err != nil {
			t.Fatal(err)
		}

		xmlHandler, err := xsdvalidate.NewXmlHandlerMem(buf.Bytes(), xsdvalidate.ParsErrVerbose)
		defer xmlHandler.Free()
		if err != nil {
			t.Fatal(err)
		}

		err = handler.Validate(xmlHandler, xsdvalidate.ParsErrVerbose)
		if err != nil {
			t.Error(err)
		}
	})
}
