// Copyright 2022 Roma Hicks

// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:

// 1. Redistributions of source code must retain the above copyright notice, this
// list of conditions and the following disclaimer.

// 2. Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.

// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

package httpserver

import (
	"encoding/xml"
	"fmt"

	geopackage "gitlab.com/feather-wfst/geopackagego"

	//"gitlab.com/feather-wfst/wfst-server/xmllibrary"
	"io/ioutil"
	"log"
	"net/http"
	"runtime"
	"strings"
	"time"
)

// WFSServerInterface is an interface tha a WFS server must adhere to.
type WFSServerInterface interface {
	GetCapabilities() string
	ListContent() []*geopackage.Content
	Uptime() time.Duration
	GeoPackagePath() string
	ListenAddress() string
	CalcLayerSize(*geopackage.Content) uint
}

// NewXMLServer is a mutex of paths that can be handled by the HTML server.
func NewXMLServer(fS WFSServerInterface) *http.Server {
	mux := http.NewServeMux()
	mux.HandleFunc("/", func(res http.ResponseWriter, req *http.Request) {
		rootRequest(res, req, fS)
	})
	mux.HandleFunc("/wfs", func(res http.ResponseWriter, req *http.Request) {
		wfsRequest(res, req, fS)
	})
	s := &http.Server{Addr: ":6000", Handler: mux}
	return s
}

// This builds the simple response to the client if they request the root.
// Contains basic information about the server.
func rootRequest(res http.ResponseWriter, req *http.Request, fS WFSServerInterface) {
	log.Printf("REQUEST: %v %v\n", req.Method, req.URL.String())

	memStat := runtime.MemStats{}
	runtime.ReadMemStats(&memStat)
	shortMem := float64(memStat.Sys) / 1048576
	fmt.Fprintf(res, "FeatherWFST SERVER\n======\n")
	fmt.Fprintf(res, "SERVER UPTIME: %v\n", fS.Uptime().Truncate(time.Second).String())
	fmt.Fprintf(res, "MEMORY USED: %.2f MiB\n", shortMem)
	fmt.Fprintf(res, "LISTEN ADDRESS: %v\n", fS.ListenAddress())
	fmt.Fprintf(res, "GEOPACKAGE IN USE: %v\n", fS.GeoPackagePath())
	fmt.Fprintf(res, "LAYERS LOADED INTO MEMORY: %v\n", len(fS.ListContent()))
	for _, l := range fS.ListContent() {
		sizeByte := fS.CalcLayerSize(l)
		fmt.Fprintf(res, "  * %v [%v features] [%.1f KiB]\n", l.TableName, len(l.Data.Rows), float64(sizeByte)/1024)
	}
}

// Entry point of all WFS requests made to the server.
func wfsRequest(res http.ResponseWriter, req *http.Request, fS WFSServerInterface) {
	log.Printf("REQUEST: %v %v\n", req.Method, req.URL.String())

	switch req.Method {
	case "GET":
		wfsGetRequest(res, req, fS)
	case "POST":
		wfsPostRequest(res, req, fS)
	}
}

// Process a GET request made to the server.
// Usually only for Capabilities requests or to view features.
func wfsGetRequest(res http.ResponseWriter, req *http.Request, fS WFSServerInterface) {
	kvp := req.URL.Query()

	var service []string
	var request []string

	for i, v := range kvp {
		i = strings.ToLower(i)
		switch i {
		case "service":
			service = v
		case "request":
			request = v
		}
	}

	if len(service) == 0 || len(request) == 0 {
		res.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(res, "Request is malformed.")
		return
	}

	if service[0] != "WFS" {
		res.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(res, "Service requested was not Web Feature Service. This server only supports Web Feature Service.")
		return
	}

	if request[0] == "GetCapabilities" {
		wfsStruct := fS.GetCapabilities()
		//fmt.Fprintf(res, xml.Header)
		fmt.Fprint(res, wfsStruct)
	}
}

// A simple generic XML object used to extract node names.
// Used to understand the SOAP method to pass on to the correct process.
type genericXMLRoot struct {
	XMLName xml.Name
}

// Process requests made to the server via POST.
// Usually done for SOAP protocols.
func wfsPostRequest(res http.ResponseWriter, req *http.Request, fS WFSServerInterface) {
	var err error
	body, err := ioutil.ReadAll(req.Body)
	if err != nil {
		panic(err)
	}
	genericXML := genericXMLRoot{}
	err = xml.Unmarshal(body, &genericXML)
	if err != nil {
		panic(err)
	}
	switch genericXML.XMLName.Local {
	case "GetCapabilities":
		wfsStruct := fS.GetCapabilities()
		//fmt.Fprintf(res, xml.Header)
		fmt.Fprint(res, wfsStruct)
		return
	default:
		res.WriteHeader(http.StatusNotImplemented)
	}
}
