// Copyright 2022 Roma Hicks

// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:

// 1. Redistributions of source code must retain the above copyright notice, this
// list of conditions and the following disclaimer.

// 2. Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.

// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

package main

import (
	"flag"
	"log"
	"os"
	"os/signal"
	"path/filepath"
	"syscall"
	"time"

	geopackage "gitlab.com/feather-wfst/geopackagego"
	"gitlab.com/feather-wfst/wfst-server/httpserver"
	"gitlab.com/feather-wfst/wfst-server/xmllibrary"
)

var gpkgPath string
var listenAddr string

func init() {
	// Check flags.
	flag.StringVar(&gpkgPath, "geopackage", "geography.gpkg", "Path to the GeoPackage used by the server.")
	flag.StringVar(&listenAddr, "listen", ":6060", "Address to listen for clients.")
}

func main() {
	flag.Parse()
	var err error
	path, _ := filepath.Abs(gpkgPath)

	// Define all the supported operands.
	geometryOperands := []string{
		"gml:Envelope",
		"gml:Point",
		"gml:LineString",
		"gml:Polygon",
		"gml:PolyhedralSurface",
		"gml:Tin",
	}
	spatialOperators := []string{
		"BBOX",
	}
	comparisonOperators := []string{
		"LessThan",
		"GreaterThan",
		"LessThanEqualTo",
		"GreaterThanEqualTo",
		"EqualTo",
		"NotEqualTo",
	}
	idOperands := []string{
		"ogc:FID",
	}

	// Define Operation Metadata
	operationsMetadata := xmllibrary.OperationsMetadata{
		[]xmllibrary.OperationMetadata{
			{"GetCapabilities",
				[]xmllibrary.OperationMethod{
					{"ows:Get", "http://127.0.0.1:6060/wfs?"},
					{"ows:Post", "http://127.0.0.1:6060/wfs"},
				},
				[]xmllibrary.NameValuePair{ // Parameters
					{"AcceptVersion", []string{"1.1.0"}},
					{"AcceptFormat", []string{"text/xml"}},
				},
				[]xmllibrary.NameValuePair{}, // Constraints
			},
			{"DescribeFeatureType",
				[]xmllibrary.OperationMethod{
					{"ows:Get", "http://127.0.0.1:6060/wfs?"},
					{"ows:Post", "http://127.0.0.1:6060/wfs"},
				},
				[]xmllibrary.NameValuePair{}, // Parameters
				[]xmllibrary.NameValuePair{}, // Constraints
			},
			{"GetFeature",
				[]xmllibrary.OperationMethod{
					{"ows:Get", "http://127.0.0.1:6060/wfs?"},
					{"ows:Post", "http://127.0.0.1:6060/wfs"},
				},
				[]xmllibrary.NameValuePair{}, // Parameters
				[]xmllibrary.NameValuePair{}, // Constraints
			},
		},
		[]xmllibrary.NameValuePair{}, // Parameters
		[]xmllibrary.NameValuePair{ // Constraints
			{"DefaultMaxFeatures", []string{"100"}},
			{"DefaultLockExpiry", []string{"5"}},
			{"SupportsSOAP", []string{"True"}},
		},
	}

	// Intialise the WFS server structure.
	fS := FeatherServer{
		databasePath:        path,
		startTime:           time.Now(),
		ServerTitle:         "FeatherWFST",
		ServerAbstract:      "A simple WFS-T server that transmits features from a GeoPackage.",
		ServiceType:         "WFS",
		ServiceTypeVersion:  "1.1.0",
		GeometryOperands:    geometryOperands,
		SpatialOperators:    spatialOperators,
		ComparisonOperators: comparisonOperators,
		IDOperands:          idOperands,
		Operations:          operationsMetadata}
	log.Printf("SERVER: Starting server.\n")

	// Pass the WFS server to XML/HTTP server.
	fS.httpServer = httpserver.NewXMLServer(&fS)
	defer fS.httpServer.Close()

	// Open and load the geopackage.
	fS.database, err = geopackage.OpenGeoPackage(fS.databasePath)
	if err != nil {
		panic(err)
	}
	defer fS.database.DB.Close()

	fS.database.LoadContent()
	if err != nil {
		panic(err)
	}

	// Start listening for requests.
	fS.httpServer.Addr = listenAddr
	go func() {
		log.Printf("SERVER: Listening on %v\n", fS.httpServer.Addr)
		err = fS.httpServer.ListenAndServe()
		if err != nil {
			panic(err)
		}
	}()

	// Listen for shutdown signals to cleanly shutdown the server.
	signalChannel := make(chan os.Signal, 3)
	signal.Notify(signalChannel, syscall.SIGTERM, syscall.SIGINT)
	for {
		sig := <-signalChannel
		if sig == syscall.SIGTERM || sig == syscall.SIGINT {
			break
		}
	}

	log.Printf("SERVER: Server stopped.\n")
}
