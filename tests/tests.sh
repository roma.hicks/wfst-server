#!/bin/sh

# WellKnownGeometry Test Suite by Roma Hicks is marked with CC0 1.0 Universal.
# To view a copy of this license, visit http://creativecommons.org/publicdomain/zero/1.0

set -x
go build -o FeatherWFST_TESTS gitlab.com/feather-wfst/wfst-server
./FeatherWFST_TESTS &
FWFST_PID=${!}
go test -count=1 ${@} gitlab.com/feather-wfst/wfst-server/tests
kill ${FWFST_PID}
set +x

